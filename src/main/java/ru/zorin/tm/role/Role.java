package ru.zorin.tm.role;
import org.springframework.stereotype.Component;

public enum Role {

    USER("User"),
    ADMIN("Admin");

    private final String displayName;

    Role(String displayName) {
        this.displayName=displayName;
    }

    public String getDisplayName() {
        return displayName;
    }
}