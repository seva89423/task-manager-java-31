package ru.zorin.tm.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan("ru.zorin.tm")
public class ApplicationConfig {

}