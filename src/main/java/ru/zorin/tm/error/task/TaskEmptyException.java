package ru.zorin.tm.error.task;

import ru.zorin.tm.error.AbstractException;

public class TaskEmptyException extends AbstractException {

    public TaskEmptyException() {
        super("Task is empty");
    }
}