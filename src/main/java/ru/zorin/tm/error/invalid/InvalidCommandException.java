package ru.zorin.tm.error.invalid;

import ru.zorin.tm.error.AbstractException;

public class InvalidCommandException extends AbstractException {

    public InvalidCommandException() {
        super("Invalid command");
    }
}