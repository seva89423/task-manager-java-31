package ru.zorin.tm.error;

import org.springframework.stereotype.Component;

public abstract class AbstractException extends RuntimeException {

    public AbstractException() {
    }

    public AbstractException(String message) {
        super(message);
    }

    public AbstractException(Throwable cause) {
        super(cause);
    }
}