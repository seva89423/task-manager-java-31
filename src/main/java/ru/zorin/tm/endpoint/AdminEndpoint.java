package ru.zorin.tm.endpoint;

import org.springframework.stereotype.Controller;
import ru.zorin.tm.api.endpoint.IAdminEndpoint;
import ru.zorin.tm.api.service.IServiceLocator;
import ru.zorin.tm.dto.Fail;
import ru.zorin.tm.dto.Result;
import ru.zorin.tm.dto.Success;
import ru.zorin.tm.entity.Session;
import ru.zorin.tm.role.Role;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService
@Controller
public class AdminEndpoint implements IAdminEndpoint {

    private final IServiceLocator serviceLocator;

    public AdminEndpoint(IServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    @WebMethod
    @Override
    public Result loadData64(@WebParam(name = "session", partName = "session") Session session) throws Exception {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        try {
            serviceLocator.getAdminService().loadData64();
            return new Success();
        } catch (Exception e) {
            return new Fail(e);
        }
    }

    @WebMethod
    @Override
    public Result saveData64(@WebParam(name = "session", partName = "session") Session session) throws Exception {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        try {
            serviceLocator.getAdminService().saveData64();
            return new Success();
        } catch (Exception e) {
            return new Fail(e);
        }
    }

    @WebMethod
    @Override
    public Result clearData64(@WebParam(name = "session", partName = "session") Session session) throws Exception {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        try {
            serviceLocator.getAdminService().clearData64();
            return new Success();
        } catch (Exception e) {
            return new Fail(e);
        }
    }

    @Override
    public Result loadBin(Session session) throws Exception {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        try {
            serviceLocator.getAdminService().loadBin();
            return new Success();
        } catch (Exception e) {
            return new Fail(e);
        }
    }

    @Override
    public Result saveBin(Session session) throws Exception {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        try {
            serviceLocator.getAdminService().saveBin();
            return new Success();
        } catch (Exception e) {
            return new Fail(e);
        }
    }

    @Override
    public Result clearBin(Session session) throws Exception {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        try {
            serviceLocator.getAdminService().clearBin();
            return new Success();
        } catch (Exception e) {
            return new Fail(e);
        }
    }

    @WebMethod
    @Override
    public Result loadFastJson(@WebParam(name = "session", partName = "session") Session session) throws Exception {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        try {
            serviceLocator.getAdminService().loadFastJson();
            return new Success();
        } catch (Exception e) {
            return new Fail(e);
        }
    }

    @WebMethod
    @Override
    public Result saveFastJson(@WebParam(name = "session", partName = "session") Session session) throws Exception {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        try {
            serviceLocator.getAdminService().saveFastJson();
            return new Success();
        } catch (Exception e) {
            return new Fail(e);
        }
    }

    @WebMethod
    @Override
    public Result clearFastJson(@WebParam(name = "session", partName = "session") Session session) throws Exception {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        try {
            serviceLocator.getAdminService().clearFastJson();
            return new Success();
        } catch (Exception e) {
            return new Fail(e);
        }
    }

    @WebMethod
    @Override
    public Result loadFastXML(@WebParam(name = "session", partName = "session") Session session) throws Exception {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        try {
            serviceLocator.getAdminService().loadFastXML();
            return new Success();
        } catch (Exception e) {
            return new Fail(e);
        }
    }

    @WebMethod
    @Override
    public Result saveFastXML(@WebParam(name = "session", partName = "session") Session session) throws Exception {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        try {
            serviceLocator.getAdminService().saveFastXML();
            return new Success();
        } catch (Exception e) {
            return new Fail(e);
        }
    }

    @WebMethod
    @Override
    public Result clearFastXML(@WebParam(name = "session", partName = "session") Session session) throws Exception {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        try {
            serviceLocator.getAdminService().clearFastXML();
            return new Success();
        } catch (Exception e) {
            return new Fail(e);
        }
    }

    @WebMethod
    @Override
    public Result loadJaxbJson(@WebParam(name = "session", partName = "session") Session session) throws Exception {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        try {
            serviceLocator.getAdminService().loadJaxbJson();
            return new Success();
        } catch (Exception e) {
            return new Fail(e);
        }
    }

    @WebMethod
    @Override
    public Result saveJaxbJson(@WebParam(name = "session", partName = "session") Session session) throws Exception {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        try {
            serviceLocator.getAdminService().saveJaxbJson();
            return new Success();
        } catch (Exception e) {
            return new Fail(e);
        }
    }

    @Override
    public Result clearJaxbJson(@WebParam(name = "session", partName = "session") Session session) throws Exception {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        try {
            serviceLocator.getAdminService().clearJaxbJson();
            return new Success();
        } catch (Exception e) {
            return new Fail(e);
        }
    }

    @WebMethod
    @Override
    public Result loadJaxbXML(@WebParam(name = "session", partName = "session") Session session) throws Exception {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        try {
            serviceLocator.getAdminService().loadJaxbXML();
            return new Success();
        } catch (Exception e) {
            return new Fail(e);
        }
    }

    @WebMethod
    @Override
    public Result saveJaxbXML(@WebParam(name = "session", partName = "session") Session session) throws Exception {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        try {
            serviceLocator.getAdminService().saveJaxbJson();
            return new Success();
        } catch (Exception e) {
            return new Fail(e);
        }
    }

    @WebMethod
    @Override
    public Result clearJaxbXML(@WebParam(name = "session", partName = "session") Session session) throws Exception {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        try {
            serviceLocator.getAdminService().clearJaxbJson();
            return new Success();
        } catch (Exception e) {
            return new Fail(e);
        }
    }
}