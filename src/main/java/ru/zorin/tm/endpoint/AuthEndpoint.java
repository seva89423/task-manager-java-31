package ru.zorin.tm.endpoint;

import org.jetbrains.annotations.Nullable;
import ru.zorin.tm.api.endpoint.IAuthEndpoint;
import ru.zorin.tm.api.service.IAuthService;
import ru.zorin.tm.entity.User;
import ru.zorin.tm.role.Role;
import ru.zorin.tm.service.AuthService;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService
public class AuthEndpoint implements IAuthEndpoint {

    private IAuthService authService;

    public AuthEndpoint() {

    }

    public AuthEndpoint(IAuthService authService) {
        this.authService = authService;
    }

    @WebMethod
    public User getUser(@WebParam(name = "login", partName = "login") @Nullable String login) throws Exception {
        return authService.getUser(login);
    }

    @WebMethod
    public boolean isAuth() {
        return authService.isAuth();
    }

    @WebMethod
    public void login(@WebParam(name = "login") @Nullable String login, @WebParam(name = "password") @Nullable String password) {
        authService.login(login, password);
    }


    @WebMethod
    public void logout() {
        authService.logout();
    }

    @WebMethod
    public void registry(@WebParam(name = "login", partName = "login") String login, @WebParam(name = "password", partName = "password") String password, @WebParam(name = "email", partName = "email") String email) throws Exception {
        authService.registry(login, password, email);
    }

    @WebMethod
    public void checkRole(@WebParam(name = "role", partName = "role") Role[] roles) throws Exception {
        authService.checkRole(roles);
    }
}