package ru.zorin.tm.endpoint;

import ru.zorin.tm.api.endpoint.IProjectEndpoint;;
import ru.zorin.tm.api.service.IServiceLocator;
import ru.zorin.tm.entity.Project;
import ru.zorin.tm.entity.Session;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@WebService
public class ProjectEndpoint implements IProjectEndpoint {

    private IServiceLocator serviceLocator;

    public ProjectEndpoint() {

    }

    public ProjectEndpoint(IServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    @WebMethod
    @Override
    public void createProject(@WebParam (name = "session", partName = "session") Session session, @WebParam(name = "name", partName = "name") String name) throws Exception {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getProjectService().create(session.getUserId(), name);
    }

    @WebMethod
    @Override
    public void addProject(@WebParam (name = "session", partName = "session") Session session, @WebParam(name = "project", partName = "project") Project project) throws Exception {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getProjectService().add(session.getUserId(), project);
    }

    @WebMethod
    @Override
    public void removeProject(@WebParam (name = "session", partName = "session") Session session, @WebParam(name = "project", partName = "project") Project project) throws Exception {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getProjectService().remove(session.getUserId(), project);
    }

    @WebMethod
    @Override
    public List<Project> findAllProject(@WebParam (name = "session", partName = "session") Session session) throws Exception {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getProjectService().findAll(session.getUserId());
    }

    @WebMethod
    @Override
    public void clearProject(@WebParam (name = "session", partName = "session") Session session) throws Exception {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getProjectService().clear(session.getUserId());
    }

    @WebMethod
    @Override
    public Project findProjectById(@WebParam (name = "session", partName = "session") Session session, @WebParam(name = "id", partName = "id") String id) throws Exception {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getProjectService().findProjectById(session.getUserId(), id);
    }

    @WebMethod
    @Override
    public Project findProjectByIndex(@WebParam (name = "session", partName = "session") Session session, @WebParam(name = "index", partName = "index") Integer index) throws Exception {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getProjectService().findProjectByIndex(session.getUserId(), index);
    }

    @WebMethod
    @Override
    public Project findProjectByName(@WebParam (name = "session", partName = "session") Session session, @WebParam(name = "name", partName = "name") String name) throws Exception {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getProjectService().findProjectByName(session.getUserId(), name);
    }

    @WebMethod
    @Override
    public Project removeProjectByName(@WebParam (name = "session", partName = "session") Session session, @WebParam(name = "name", partName = "name") String name) throws Exception {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getProjectService().removeProjectByName(session.getUserId(), name);
    }

    @WebMethod
    @Override
    public Project removeProjectByIndex(@WebParam (name = "session", partName = "session") Session session, @WebParam(name = "index", partName = "index") Integer index) throws Exception {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getProjectService().removeProjectByIndex(session.getUserId(), index);
    }

    @WebMethod
    @Override
    public Project removeProjectById(@WebParam (name = "session", partName = "session") Session session, @WebParam(name = "id", partName = "id") String id) throws Exception {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getProjectService().removeProjectById(session.getUserId(), id);
    }

    @WebMethod
    @Override
    public Project updateProjectById(@WebParam (name = "session", partName = "session") Session session, @WebParam(name = "id", partName = "id") String id, String name, String description) throws Exception {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getProjectService().updateProjectById(session.getUserId(), id, name, description);
    }

    @WebMethod
    @Override
    public Project updateProjectByIndex(@WebParam (name = "session", partName = "session") Session session, @WebParam(name = "index", partName = "index") Integer index, @WebParam(name = "name", partName = "name") String name, @WebParam(name = "description", partName = "description") String description) throws Exception {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getProjectService().updateProjectByIndex(session.getUserId(), index, name, description);
    }
}