package ru.zorin.tm.endpoint;

import org.jetbrains.annotations.Nullable;
import ru.zorin.tm.api.endpoint.ISessionEndpoint;
import ru.zorin.tm.api.service.IServiceLocator;
import ru.zorin.tm.api.service.ISessionService;
import ru.zorin.tm.dto.Fail;
import ru.zorin.tm.dto.Result;
import ru.zorin.tm.dto.Success;
import ru.zorin.tm.entity.Session;
import ru.zorin.tm.entity.User;
import ru.zorin.tm.role.Role;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@WebService
public class SessionEndpoint implements ISessionEndpoint {

    private IServiceLocator serviceLocator;

    public SessionEndpoint(IServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    @WebMethod
    @Override
    public Result closeSession(@WebParam(name = "session", partName = "session") @Nullable Session session) throws Exception {
        serviceLocator.getSessionService().validate(session);
        try {
            serviceLocator.getSessionService().close(session);
            return new Success();
        } catch (Exception e) {
            return new Fail(e);
        }
    }

    @WebMethod
    @Override
    public Result closeAllSession(@WebParam(name = "session", partName = "session") @Nullable Session session) throws Exception {
        serviceLocator.getSessionService().validate(session);
        try {
            serviceLocator.getSessionService().closeAll(session);
            return new Success();
        } catch (Exception e) {
            return new Fail(e);
        }
    }

    @WebMethod
    @Override
    public @Nullable Session openSession(@WebParam(name = "login", partName = "login") @Nullable String login, @WebParam(name = "password", partName = "password") @Nullable String password) throws Exception {
        return serviceLocator.getSessionService().open(login, password);
    }
}