package ru.zorin.tm.entity;

import lombok.Getter;
import lombok.Setter;
import org.springframework.stereotype.Component;
import ru.zorin.tm.role.Role;

import java.io.Serializable;
@Getter
@Setter
@Component
public class User extends AbstractEntity implements Serializable {

    private String login;

    private String password;

    private String email;

    private String firstName;

    private String lastName;

    private String middleName;

    private Role role = Role.USER;

    private Boolean locked = false;

    public String toString() {
        return getId()  +": "+login;
    }
}