package ru.zorin.tm.entity;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;
import org.springframework.stereotype.Component;

@Getter
@Setter
@JsonIgnoreProperties(ignoreUnknown = true)
@Component
public class Session extends AbstractEntity implements Cloneable {

    private Long timestamp;
    private String userId;
    private String signature;

    public Session() {

    }

    public Session clone() {
        try {
            return (Session) super.clone();
        } catch (CloneNotSupportedException e) {
            return null;
        }
    }
}