package ru.zorin.tm.command.system;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.zorin.tm.bootstrap.Bootstrap;
import ru.zorin.tm.command.AbstractCommand;

import java.util.Collection;

public final class HelpCommand extends AbstractCommand {

    @Override
    public String arg() {
        return "-h";
    }

    @NotNull
    @Override
    public String name() {
        return "help";
    }

    @NotNull
    @Override
    public String description() {
        return "Show commands";
    }

    @Nullable
    @Override
    public void execute() throws Exception {
        System.out.println("[HELP]");
        final Bootstrap bootstrap = (Bootstrap) IServiceLocator;
        final Collection<AbstractCommand> commands = bootstrap.getCommands();
        final Collection<AbstractCommand> arguments = bootstrap.getArgs();
        for (AbstractCommand command:commands){
            if (command.arg() != null){
            System.out.println(command.name() + " ("+ command.arg() +") : " + command.description());
            } else{
                System.out.println(command.name() + " : " + command.description());
            }
        }
        System.out.println("[COMPLETE]");
    }
}