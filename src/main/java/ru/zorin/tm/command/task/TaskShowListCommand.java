package ru.zorin.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.zorin.tm.command.AbstractCommand;
import ru.zorin.tm.entity.Task;

import java.util.List;

public class TaskShowListCommand extends AbstractCommand {
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "task-list";
    }

    @NotNull
    @Override
    public String description() {
        return "Show list of tasks";
    }

    @Nullable
    @Override
    public void execute() throws Exception {
        final String userId = IServiceLocator.getAuthService().getUserId();
        System.out.println("[LIST TASKS]");
        final List<Task> tasks = IServiceLocator.getTaskService().findAll(userId);
        for (Task task : tasks) System.out.println(task);
        System.out.println("[COMPLETE]");
    }
}