package ru.zorin.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.zorin.tm.command.AbstractCommand;
import ru.zorin.tm.role.Role;
import ru.zorin.tm.util.TerminalUtil;

public class TaskCreateCommand extends AbstractCommand {
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "task-create-new";
    }

    @NotNull
    @Override
    public String description() {
        return "Create new task";
    }

    @Nullable
    @Override
    public void execute() throws Exception {
        final String userId = IServiceLocator.getAuthService().getUserId();
        System.out.println("[CREATE TASK]");
        System.out.println("[ENTER NAME]");
        final String name = TerminalUtil.nextLine();
        System.out.println("[ENTER DESCRIPTION]");
        final String description = TerminalUtil.nextLine();
        IServiceLocator.getTaskService().create(userId, name, description);
        System.out.println("[COMPLETE]");
    }

    @NotNull
    public Role[] roles() {
        return new Role[] { Role.USER, Role.ADMIN };
    }
}