package ru.zorin.tm.command;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.zorin.tm.dto.Domain;

import java.io.Serializable;

public abstract class AbstractDataCommand extends AbstractCommand implements Serializable {

    public AbstractDataCommand() {

    }

    @Nullable
    public Domain getDomain() {
        final Domain domain = new Domain();
        domain.setProjects(IServiceLocator.getProjectService().getList());
        domain.setTasks(IServiceLocator.getTaskService().getList());
        domain.setUsers(IServiceLocator.getUserService().getList());
        return domain;
    }

    @Nullable
    public void setDomain(final Domain domain) {
        if (domain == null) return;
        IServiceLocator.getProjectService().getList().clear();
        IServiceLocator.getProjectService().load(domain.getProjects());
        IServiceLocator.getTaskService().getList().clear();
        IServiceLocator.getTaskService().load(domain.getTasks());
        IServiceLocator.getUserService().getList().clear();
        IServiceLocator.getUserService().load(domain.getUsers());
    }

    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return null;
    }

    @Nullable
    @Override
    public void execute() throws Exception {

    }
}
