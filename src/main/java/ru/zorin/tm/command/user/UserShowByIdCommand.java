package ru.zorin.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.zorin.tm.command.AbstractCommand;
import ru.zorin.tm.entity.User;
import ru.zorin.tm.role.Role;
import ru.zorin.tm.util.TerminalUtil;

public class UserShowByIdCommand extends AbstractCommand {

    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "show-user-by-id";
    }

    @NotNull
    @Override
    public String description() {
        return "Show information about user by id";
    }

    @Nullable
    @Override
    public void execute() throws Exception {
        final String userId = IServiceLocator.getAuthService().getUserId();
        System.out.println("[SHOW USER]");
        System.out.println("ENTER ID:");
        final String id = TerminalUtil.nextLine();
        final User user = IServiceLocator.getUserService().findById(id);
        if (user == null) {
            System.out.println("[ERROR]");
            return;
        }
        System.out.println("ID: " + user.getId());
        System.out.println("LOGIN: " + user.getLogin());
        System.out.println("[COMPLETE]");
    }

    @NotNull
    public Role[] roles() {
        return new Role[] { Role.ADMIN };
    }
}
