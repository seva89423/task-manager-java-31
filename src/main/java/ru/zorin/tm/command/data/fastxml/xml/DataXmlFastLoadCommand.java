package ru.zorin.tm.command.data.fastxml.xml;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.zorin.tm.constant.DataConst;
import ru.zorin.tm.command.AbstractDataCommand;
import ru.zorin.tm.dto.Domain;
import ru.zorin.tm.role.Role;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

public class DataXmlFastLoadCommand extends AbstractDataCommand {
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "data-xml-fast-load";
    }

    @NotNull
    @Override
    public String description() {
        return "Load data from xml (FasterXML) file";
    }

    @Nullable
    @Override
    public void execute() throws IOException {
        System.out.println("[DATA XML (FAST) LOAD]");
        final String data = new String(Files.readAllBytes(Paths.get(DataConst.FILE_XML_FAST)));
        final ObjectMapper objectMapper = new XmlMapper();
        final Domain domain = (Domain) objectMapper.readValue(data, Domain.class);
        setDomain(domain);
        System.out.println("[COMPLETE]");
    }

    public Role[] roles(){
        return new Role[] { Role.ADMIN };
    }
}