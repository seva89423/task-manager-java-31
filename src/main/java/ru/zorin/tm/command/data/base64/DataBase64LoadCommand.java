package ru.zorin.tm.command.data.base64;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.zorin.tm.command.AbstractDataCommand;
import ru.zorin.tm.constant.DataConst;
import ru.zorin.tm.dto.Domain;
import ru.zorin.tm.role.Role;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Base64;

public class DataBase64LoadCommand extends AbstractDataCommand {
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "data-base64-load";
    }

    @NotNull
    @Override
    public String description() {
        return "Load data from base64 file";
    }

    @Nullable
    @Override
    public void execute() throws Exception {
        System.out.println("[DATA BASE64 LOAD]");
        final String base64dec = new String(Files.readAllBytes(Paths.get(DataConst.FILE_BASE64)));
        final byte[] base64 = Base64.getDecoder().decode(base64dec);
        final ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(base64);
        final ObjectInputStream objectInputStream = new ObjectInputStream(byteArrayInputStream);
        final Domain domain = (Domain) objectInputStream.readObject();
        setDomain(domain);
        System.out.println("[COMPLETE]");
    }

    public Role[] roles(){
        return new Role[] { Role.ADMIN };
    }
}