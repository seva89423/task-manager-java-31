package ru.zorin.tm.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.zorin.tm.api.repository.ICommandRepository;
import ru.zorin.tm.api.service.ICommandService;
import ru.zorin.tm.command.AbstractCommand;

import java.util.List;

@Service
public class CommandService implements ICommandService {

    @Autowired
    private ICommandRepository commandRepository;

    public CommandService(final ICommandRepository commandRepository) {
        this.commandRepository = commandRepository;
    }

    @Override
    public List<AbstractCommand> getTermCommands() {
        return commandRepository.getTermCommands();
    }
}