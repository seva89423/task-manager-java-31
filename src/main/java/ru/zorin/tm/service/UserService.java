package ru.zorin.tm.service;

import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.zorin.tm.api.repository.IUserRepository;
import ru.zorin.tm.api.service.IUserService;
import ru.zorin.tm.entity.User;
import ru.zorin.tm.error.access.AccessDeniedException;
import ru.zorin.tm.error.invalid.*;
import ru.zorin.tm.error.task.TaskEmptyException;
import ru.zorin.tm.role.Role;
import ru.zorin.tm.util.HashUtil;

import java.util.List;

@Service
public class UserService implements IUserService {

    private final IUserRepository userRepository;

    private String userId;

    @Autowired
    public UserService(final IUserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public List<User> findAll() {
        return userRepository.findAllUser();
    }

    @Override
    public User findById(final String userId) {
        if (userId == null || userId.isEmpty()) throw new InvalidUserIdException();
        return userRepository.findByUserId(userId);
    }


    @Override
    public void load(final List<User> usersList) {
        if (usersList == null) return;
        userRepository.load(usersList);
    }

    public String getUserId() {
        if (userId == null) throw new AccessDeniedException();
        return userId;
    }

    @Override
    public List<User> getList() {
        return userRepository.getUsersList();
    }


    @Override
    public User findByLogin(final String login) {
        if (login == null || login.isEmpty()) throw new InvalidLoginException();
        return userRepository.findByUserLogin(login);
    }

    @Override
    public User removeUser(final User user) {
        if (user == null) return null;
        return userRepository.removeUser(user);
    }

    @Override
    public User removeById(final String id) {
        if (id == null || id.isEmpty()) throw new InvalidIdException();
        return userRepository.removeByUserId(id);
    }

    @Override
    public User removeByLogin(final String login) {
        if (login == null || login.isEmpty()) throw new InvalidLoginException();
        return userRepository.removeByUserLogin(login);
    }

    @Nullable
    @Override
    public User create(final String login, final String password) {
        if (login == null || login.isEmpty()) throw new InvalidLoginException();
        if (password == null || password.isEmpty()) throw new InvalidPasswordException();
        final User user = new User();
        user.setRole(Role.USER);
        user.setLogin(login);
        user.setPassword(HashUtil.salt(password));
        return userRepository.add(user);
    }

    @Nullable
    @Override
    public User create(final String login, final String password, final String email) {
        if (login == null || login.isEmpty()) throw new InvalidLoginException();
        if (password == null || password.isEmpty()) throw new InvalidPasswordException();
        if (email == null || email.isEmpty()) throw new InvalidEmailException();
        final User user = create(login, password);
        if (user == null) return null;
        user.setEmail(email);
        return user;
    }

    @Nullable
    @Override
    public User create(final String login, final String password, final Role role) {
        if (login == null || login.isEmpty()) throw new InvalidLoginException();
        if (password == null || password.isEmpty()) throw new InvalidPasswordException();
        if (role == null) throw new InvalidRoleException();
        final User user = create(login, password);
        if (user == null) return null;
        user.setRole(role);
        return user;
    }

    @Nullable
    @Override
    public User updateById(final String id, final String login, final String password) {
        if (id == null || id.isEmpty()) throw new InvalidIdException();
        if (login == null || login.isEmpty()) throw new InvalidNameException();
        final User user = findById(id);
        if (user == null) throw new TaskEmptyException();
        user.setId(id);
        user.setLogin(login);
        user.setPassword(password);
        return user;
    }

    @Nullable
    @Override
    public User updateByLogin(final String login, final String newlogin, final String password) {
        if (login == null || login.isEmpty()) throw new InvalidLoginException();
        if (newlogin == null || newlogin.isEmpty()) throw new InvalidNameException();
        final User user = findByLogin(login);
        if (user == null) throw new TaskEmptyException();
        user.setLogin(newlogin);
        user.setPassword(password);
        return user;
    }

    @Nullable
    @Override
    public User lockServiceByLogin(String login) {
        if (login == null || login.isEmpty()) throw new InvalidNameException();
        final User user = findByLogin(login);
        if (user == null) throw new InvalidNameException();
        user.setLocked(true);
        return user;
    }

    @Nullable
    @Override
    public User unlockServiceByLogin(String login) {
        if (login == null || login.isEmpty()) throw new InvalidNameException();
        final User user = findByLogin(login);
        if (user == null) throw new InvalidNameException();
        user.setLocked(false);
        return user;
    }
}