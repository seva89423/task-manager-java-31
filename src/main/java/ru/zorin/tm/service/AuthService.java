package ru.zorin.tm.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.zorin.tm.api.service.IAuthService;
import ru.zorin.tm.api.service.IUserService;
import ru.zorin.tm.entity.User;
import ru.zorin.tm.error.access.AccessDeniedException;
import ru.zorin.tm.error.invalid.InvalidLoginException;
import ru.zorin.tm.error.invalid.InvalidPasswordException;
import ru.zorin.tm.role.Role;
import ru.zorin.tm.util.HashUtil;

@Service
public final class AuthService implements IAuthService {

    private final IUserService userService;

    private String userId;

    @Override
    public User getUser(String login) {
        if (login == null || login.isEmpty()) throw new InvalidLoginException();
        User user = userService.findByLogin(login);
        if (user == null) throw new AccessDeniedException();
        return user;
    }

    @Autowired
    public AuthService(IUserService userService) {
        this.userService = userService;
    }

    public String getUserId() {
        if (userId == null) throw new AccessDeniedException();
        return userId;
    }

    @Override
    public boolean isAuth() {
        return userId == null;
    }

    @Override
    public void login(String login, String password) {
        if (login == null || login.isEmpty()) throw new InvalidLoginException();
        if (password == null || password.isEmpty()) throw new InvalidPasswordException();
        final User user = userService.findByLogin(login);
        if (user == null) throw new InvalidLoginException();
        if (user.getLocked()) throw new AccessDeniedException();
        final String hash = HashUtil.salt(password);
        if (hash == null) throw new InvalidPasswordException();
        if(!hash.equals(user.getPassword())) throw new InvalidPasswordException();
        userId = user.getId();
    }

    @Override
    public void logout() {
        userId = null;
    }

    @Override
    public void registry(String login, String password, String email) {
        userService.create(login, password, email);
    }

    @Override
    public void checkRole(Role[] roles) {
        if (roles == null || roles.length == 0) return;
        final User user = userService.findById(userId);
        final Role role = user.getRole();
        if (role == null) throw new AccessDeniedException();
        for (final Role item: roles) if(role.equals(item)) return;
        throw new AccessDeniedException();
    }
}