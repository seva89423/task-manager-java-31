package ru.zorin.tm.service;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.zorin.tm.api.repository.IProjectRepository;
import ru.zorin.tm.api.service.IProjectService;
import ru.zorin.tm.entity.Project;
import ru.zorin.tm.error.invalid.InvalidIdException;
import ru.zorin.tm.error.invalid.InvalidIndexException;
import ru.zorin.tm.error.invalid.InvalidNameException;
import ru.zorin.tm.error.invalid.InvalidUserIdException;
import ru.zorin.tm.error.project.ProjectEmptyException;

import java.util.List;

@Service
public class ProjectService extends AbstractService<Project> implements IProjectService {

    final private IProjectRepository projectRepository;

    @Autowired
    public ProjectService(@NotNull IProjectRepository projectRepository) {
        super(projectRepository);
        this.projectRepository = projectRepository;
    }

    @Override
    public void create(final String userId, final String name) {
        if (userId == null || userId.isEmpty()) throw new InvalidUserIdException();
        final Project project = new Project();
        project.setName(name);
        projectRepository.add(userId, project);
    }

    @Override
    public void create(final String userId, final String name, final String description) {
        if (userId == null || userId.isEmpty()) throw new InvalidUserIdException();
        final Project project = new Project();
        project.setName(name);
        project.setDescription(description);
        projectRepository.add(userId, project);
    }

    @Override
    public void add(final String userId, final Project project) {
        if (userId == null || userId.isEmpty()) throw new InvalidUserIdException();
        if (project == null) throw new ProjectEmptyException();
        projectRepository.add(userId, project);
    }

    @Override
    public void remove(final String userId, final Project project) {
        if (userId == null || userId.isEmpty()) throw new InvalidUserIdException();
        if (project == null) throw new ProjectEmptyException();
        projectRepository.remove(userId, project);
    }

    @Override
    public List<Project> findAll(final String userId) {
        if (userId == null || userId.isEmpty()) throw new InvalidUserIdException();
        return projectRepository.findAllProjects(userId);
    }

    public List<Project> getList() {
        return projectRepository.findAll();
    }

    @Override
    public void clear(final String userId) {
        if (userId == null || userId.isEmpty()) throw new InvalidUserIdException();
        projectRepository.clear();
    }

    @Override
    public Project findProjectById(final String userId, final String id) {
        if (userId == null || userId.isEmpty()) throw new InvalidUserIdException();
        if (id == null || id.isEmpty()) throw new InvalidIdException();
        return projectRepository.findProjectById(userId, id);
    }

    @Override
    public Project findProjectByIndex(final String userId, final Integer index) {
        if (userId == null || userId.isEmpty()) throw new InvalidUserIdException();
        if (index == null || index < 0) throw new InvalidIndexException();
        return projectRepository.findProjectByIndex(userId, index);
    }

    @Override
    public Project findProjectByName(final String userId, final String name) {
        if (userId == null || userId.isEmpty()) throw new InvalidUserIdException();
        if (name == null || name.isEmpty()) throw new InvalidNameException();
        return projectRepository.getProjectByName(userId, name);
    }

    @Override
    public Project updateProjectById(final String userId, final String id, final String name, final String description) {
        if (userId == null || userId.isEmpty()) throw new InvalidUserIdException();
        if (id == null || id.isEmpty()) throw new InvalidIdException();
        if (name == null || name.isEmpty()) throw new InvalidNameException();
        final Project project = findProjectById(userId, id);
        if (project == null) throw new ProjectEmptyException();
        project.setId(id);
        project.setName(name);
        project.setDescription(description);
        return project;
    }

    @Override
    public Project removeProjectByName(final String userId, final String name) {
        if (userId == null || userId.isEmpty()) throw new InvalidUserIdException();
        if (name == null || name.isEmpty()) throw new InvalidNameException();
        final Project project = findProjectByName(userId, name);
        if (project == null) throw new ProjectEmptyException();
        return projectRepository.removeProjectByName(userId, name);
    }

    @Override
    public Project removeProjectByIndex(final String userId, final Integer index) {
        if (userId == null || userId.isEmpty()) throw new InvalidUserIdException();
        if (index == null || index < 0) throw new InvalidIndexException();
        final Project project = findProjectByIndex(userId, index);
        if (project == null) throw new ProjectEmptyException();
        return projectRepository.removeProjectByIndex(userId, index);
    }

    @Override
    public Project removeProjectById(final String userId, final String id) {
        if (userId == null || userId.isEmpty()) throw new InvalidUserIdException();
        if (id == null || id.isEmpty()) throw new InvalidIdException();
        return projectRepository.removeProjectById(userId, id);
    }

    @Override
    public Project updateProjectByIndex(final String userId, final Integer index, final String name, final String description) {
        if (userId == null || userId.isEmpty()) throw new InvalidUserIdException();
        if (index == null || index < 0) throw new InvalidIndexException();
        if (name == null || name.isEmpty()) throw new InvalidNameException();
        final Project project = findProjectByIndex(userId, index);
        if (project == null) throw new ProjectEmptyException();
        project.setName(name);
        project.setDescription(description);
        return project;
    }

    @Override
    public void load(List<Project> projects) {
        if (projects == null) return;
        projectRepository.load(projects);
    }
}