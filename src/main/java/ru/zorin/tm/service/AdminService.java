package ru.zorin.tm.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import org.eclipse.persistence.jaxb.MarshallerProperties;
import org.eclipse.persistence.jaxb.UnmarshallerProperties;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.zorin.tm.api.service.IAdminService;
import ru.zorin.tm.api.service.IServiceLocator;
import ru.zorin.tm.constant.DataConst;
import ru.zorin.tm.dto.Domain;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Base64;

@Service
public class AdminService implements IAdminService {

    private IServiceLocator serviceLocator;

    @Autowired
    public AdminService(IServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    @Override
    public void loadData64() throws Exception {
        final String base64dec = new String(Files.readAllBytes(Paths.get(DataConst.FILE_BASE64)));
        final byte[] base64 = Base64.getDecoder().decode(base64dec);
        final ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(base64);
        final ObjectInputStream objectInputStream = new ObjectInputStream(byteArrayInputStream);
        final Domain domain = (Domain) objectInputStream.readObject();
        serviceLocator.getDomainService().load(domain);
        objectInputStream.close();
    }

    @Override
    public void saveData64() throws Exception {
        final Domain domain = new Domain();
        serviceLocator.getDomainService().export(domain);
        final ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        final ObjectOutputStream objectOutputStream = new ObjectOutputStream(byteArrayOutputStream);
        objectOutputStream.writeObject(domain);
        final byte[] bytes = byteArrayOutputStream.toByteArray();
        final String base64 = Base64.getEncoder().encodeToString(bytes);
        byteArrayOutputStream.close();
        final File file = new File(DataConst.FILE_BASE64);
        Files.deleteIfExists(file.toPath());
        Files.createFile(file.toPath());
        final FileOutputStream fileOutputStream = new FileOutputStream(file);
        fileOutputStream.write(base64.getBytes());
        objectOutputStream.close();
        fileOutputStream.flush();
        fileOutputStream.close();
    }

    @Override
    public void clearData64() throws Exception {
        final File file = new File(DataConst.FILE_BASE64);
        Files.deleteIfExists(file.toPath());
    }

    @Override
    public void loadBin() throws Exception {
        final FileInputStream fileInputStream = new FileInputStream(DataConst.FILE_BIN);
        final ObjectInputStream objectInputStream = new ObjectInputStream(fileInputStream);
        final Domain domain = (Domain) objectInputStream.readObject();
        serviceLocator.getDomainService().load(domain);
        fileInputStream.close();
    }

    @Override
    public void saveBin() throws Exception {
        final Domain domain = new Domain();
        serviceLocator.getDomainService().export(domain);
        final File file = new File(DataConst.FILE_BIN);
        Files.deleteIfExists(file.toPath());
        Files.createFile(file.toPath());
        final FileOutputStream fileOutputStream = new FileOutputStream(file);
        final ObjectOutputStream objectOutputStream = new ObjectOutputStream(fileOutputStream);
        objectOutputStream.writeObject(domain);
        objectOutputStream.flush();
        fileOutputStream.close();
    }

    @Override
    public void clearBin() throws Exception {
        final File file = new File(DataConst.FILE_BASE64);
        Files.deleteIfExists(file.toPath());
    }

    @Override
    public void loadFastJson() throws Exception {
        final Domain domain = new Domain();
        serviceLocator.getDomainService().export(domain);
        final File file = new File(DataConst.FILE_JSON_FAST);
        Files.deleteIfExists(file.toPath());
        Files.createFile(file.toPath());
        final ObjectMapper objectMapper = new ObjectMapper();
        final String json = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(domain);
        final FileOutputStream fileOutputStream = new FileOutputStream(file);
        fileOutputStream.write(json.getBytes());
        fileOutputStream.flush();
        fileOutputStream.close();
    }

    @Override
    public void saveFastJson() throws Exception {
        final String data = new String(Files.readAllBytes(Paths.get(DataConst.FILE_JSON_FAST)));
        final ObjectMapper objectMapper = new ObjectMapper();
        final Domain domain = (Domain) objectMapper.readValue(data, Domain.class);
        serviceLocator.getDomainService().load(domain);
    }

    @Override
    public void clearFastJson() throws Exception {
        final File file = new File(DataConst.FILE_JSON_FAST);
        Files.deleteIfExists(file.toPath());
    }

    @Override
    public void loadFastXML() throws Exception {
        final String data = new String(Files.readAllBytes(Paths.get(DataConst.FILE_XML_FAST)));
        final ObjectMapper objectMapper = new XmlMapper();
        final Domain domain = (Domain) objectMapper.readValue(data, Domain.class);
    }

    @Override
    public void saveFastXML() throws Exception {
        final Domain domain = new Domain();
        serviceLocator.getDomainService().export(domain);
        final File file = new File(DataConst.FILE_XML_FAST);
        Files.deleteIfExists(file.toPath());
        Files.createFile(file.toPath());
        final ObjectMapper objectMapper = new XmlMapper();
        final String xml = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(domain);
        final FileOutputStream fileOutputStream = new FileOutputStream(file);
        fileOutputStream.write(xml.getBytes());
        fileOutputStream.flush();
        fileOutputStream.close();
    }

    @Override
    public void clearFastXML() throws Exception {
        final File file = new File(DataConst.FILE_XML_FAST);
        Files.deleteIfExists(file.toPath());
    }

    @Override
    public void loadJaxbJson() throws Exception {
        System.setProperty("javax.xml.bind.context.factory", "org.eclipse.persistence.jaxb.JAXBContextFactory");
        JAXBContext jaxbContext = JAXBContext.newInstance(Domain.class);
        Unmarshaller unmarsh = jaxbContext.createUnmarshaller();
        final File file = new File(DataConst.FILE_JSON);
        unmarsh.setProperty(UnmarshallerProperties.MEDIA_TYPE, "application/json");
        unmarsh.setProperty(UnmarshallerProperties.JSON_INCLUDE_ROOT, true);
        Domain domain = (Domain) unmarsh.unmarshal(file);
    }

    @Override
    public void saveJaxbJson() throws Exception {
        System.setProperty("javax.xml.bind.context.factory", "org.eclipse.persistence.jaxb.JAXBContextFactory");
        final Domain domain = new Domain();
        serviceLocator.getDomainService().export(domain);
        final File file = new File(DataConst.FILE_JSON);
        Files.deleteIfExists(file.toPath());
        Files.createFile(file.toPath());
        JAXBContext jaxbContext = JAXBContext.newInstance(Domain.class);
        Marshaller marshaller = jaxbContext.createMarshaller();
        marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
        marshaller.setProperty(MarshallerProperties.MEDIA_TYPE, "application/json");
        marshaller.setProperty(MarshallerProperties.JSON_INCLUDE_ROOT, true);
        marshaller.marshal(domain, new File(DataConst.FILE_JSON));
    }

    @Override
    public void clearJaxbJson() throws Exception {
        final File file = new File(DataConst.FILE_JSON_FAST);
        Files.deleteIfExists(file.toPath());
    }

    @Override
    public void loadJaxbXML() throws Exception {
        System.setProperty("javax.xml.bind.context.factory", "org.eclipse.persistence.jaxb.JAXBContextFactory");
        final Domain domain = new Domain();
        serviceLocator.getDomainService().export(domain);
        final File file = new File(DataConst.FILE_XML_FAST);
        Files.deleteIfExists(file.toPath());
        Files.createFile(file.toPath());
        JAXBContext context = JAXBContext.newInstance(Domain.class);
        Marshaller mar = context.createMarshaller();
        mar.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
        mar.marshal(domain, new File(DataConst.FILE_XML_FAST));
    }

    @Override
    public void saveJaxbXML() throws Exception {
        System.setProperty("javax.xml.bind.context.factory", "org.eclipse.persistence.jaxb.JAXBContextFactory");
        final File file = new File(DataConst.FILE_XML_FAST);
        JAXBContext jaxbContext = JAXBContext.newInstance(Domain.class);
        Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
        Domain domain = (Domain) unmarshaller.unmarshal(file);
    }

    @Override
    public void clearJaxbXML() throws Exception {
        final File file = new File(DataConst.FILE_XML_FAST);
        Files.deleteIfExists(file.toPath());
    }
}