package ru.zorin.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.stereotype.Component;
import ru.zorin.tm.api.repository.IRepository;
import ru.zorin.tm.api.service.IService;
import ru.zorin.tm.entity.AbstractEntity;

import java.util.List;

@Component
public abstract class AbstractService<E extends AbstractEntity> implements IService<E> {

    @NotNull
    @Autowired
    private final IRepository<E> iRepository;

    public AbstractService(@NotNull final IRepository<E> repository) {
        this.iRepository = repository;
    }

    @Nullable
    @Override
    public void load(@Nullable List<E> list) {
        if (list == null) return;
        iRepository.load(list);
    }

    public List<E> getList() {
        return iRepository.findAll();
    }
}
