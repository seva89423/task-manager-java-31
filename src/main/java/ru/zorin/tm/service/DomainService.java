package ru.zorin.tm.service;

import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.zorin.tm.api.service.*;
import ru.zorin.tm.dto.Domain;

@Service
public class DomainService implements IDomainService {

    private final IUserService userService;

    private final ITaskService taskService;

    private final IProjectService projectService;

    @Autowired
    public DomainService(IUserService userService, ITaskService taskService, IProjectService projectService) {
        this.userService = userService;
        this.taskService = taskService;
        this.projectService = projectService;
    }

    @Override
    public void load(Domain domain) {
        if (domain == null) return;
    }

    @Nullable
    @Override
    public void export(@Nullable Domain domain) {
        if (domain == null) return;
        domain.setProjects(projectService.getList());
        domain.setTasks(taskService.getList());
        domain.setUsers(userService.getList());
    }
}