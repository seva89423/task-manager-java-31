package ru.zorin.tm.repository;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.reflections.Reflections;
import org.springframework.stereotype.Repository;
import ru.zorin.tm.api.repository.ICommandRepository;
import ru.zorin.tm.command.AbstractCommand;

import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

@SuppressWarnings({"deprecation", "unchecked"})
@Repository
public class CommandRepository implements ICommandRepository {

    final private List<AbstractCommand> commandList = new ArrayList<>();
    {
        initCommand();
    }

    @NotNull
    @SneakyThrows
    private void initCommand() {
        @NotNull final Reflections reflections = new Reflections("ru.zorin.tm.command");
        @NotNull final Set<Class<? extends AbstractCommand>> classes = reflections.getSubTypesOf(ru.zorin.tm.command.AbstractCommand.class);
        for (@NotNull final Class<? extends AbstractCommand> clazz : classes) {
            final boolean isAbstract = Modifier.isAbstract(clazz.getModifiers());
            if (isAbstract) continue;
            commandList.add(clazz.newInstance());
        }
    }

    @Override
    public List<AbstractCommand> getTermCommands() {
        return commandList;
    }
}