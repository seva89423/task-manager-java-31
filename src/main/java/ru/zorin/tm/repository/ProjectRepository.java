package ru.zorin.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Repository;
import ru.zorin.tm.api.repository.IProjectRepository;
import ru.zorin.tm.error.invalid.InvalidUserIdException;
import ru.zorin.tm.error.project.ProjectEmptyException;
import ru.zorin.tm.entity.Project;

import java.util.ArrayList;
import java.util.List;
@Repository
public class ProjectRepository extends AbstractRepository<Project> implements IProjectRepository  {

    @NotNull
    private List<Project> projects = new ArrayList<>();

    @Override
    public void add(@NotNull final String userId, Project project) {
        project.setUserId(userId);
        projects.add(project);
    }

    @Override
    public Project add(Project project) {
        if (project == null) return null;
        records.add(project);
        return project;
    }

    @Override
    public List<Project> findAll() {
        return projects;
    }

    @Override
    public void add(@NotNull final List<Project> project) {
        for (final Project prt : project) {
            if (prt == null) return;
            projects.add(prt);
        }
    }

    @Override
    public void load(final List<Project> projects) {
        add(projects);
    }


    @Override
    public void remove(final String userId, Project project) {
        if (!userId.equals((project.getId()))) return;
        projects.remove(project);
    }

    @Override
    public List<Project> findAllProjects(final String userId) {
        final List<Project> result = new ArrayList<>();
        for (Project project:projects) {
            if (userId.equals(project.getUserId())) result.add(project);
        }
        return result;
    }

    @Override
    public void clear() {
        final List<Project> projects = findAll();
        projects.clear();
    }

    @Override
    public Project findProjectByIndex(@NotNull final String userId, final Integer index) {
        return projects.get(index);
    }

    @Override
    public Project getProjectByName(@NotNull final String userId, final String name) {
        if (userId == null || userId.isEmpty()) throw new InvalidUserIdException();
        for (final Project project:projects){
            if (name.equals(project.getName())) return project;
        }
        return null;
    }

    @Override
    public Project removeProjectByName(@NotNull final String userId, final String name) {
        if (userId == null || userId.isEmpty()) throw new InvalidUserIdException();
        final Project project = getProjectByName(userId, name);
        if (project == null) throw new ProjectEmptyException();
        remove(userId, project);
        return project;
    }

    @Override
    public Project removeProjectByIndex(@NotNull final String userId, final Integer index) {
        if (userId == null || userId.isEmpty()) throw new InvalidUserIdException();
        final Project project = findProjectByIndex(userId, index);
        if (project == null) throw new ProjectEmptyException();
        remove(userId, project);
        return project;
    }

    @Override
    public Project findProjectById(@NotNull final String userId, final String id) {
        if (userId == null || userId.isEmpty()) throw new InvalidUserIdException();
        for (final Project task:projects){
            if (id.equals(task.getId())) return task;
        }
        return null;
    }

    @Override
    public Project removeProjectById(@NotNull final String userId, final String id) {
        if (userId == null || userId.isEmpty()) throw new InvalidUserIdException();
        final Project project = findProjectById(userId, id);
        if (project == null) throw new ProjectEmptyException();
        projects.remove(project);
        return project;
    }
}