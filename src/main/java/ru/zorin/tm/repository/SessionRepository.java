package ru.zorin.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Repository;
import ru.zorin.tm.api.repository.ISessionRepository;
import ru.zorin.tm.entity.Session;

import java.util.ArrayList;
import java.util.List;
@Repository
public class SessionRepository extends AbstractRepository<Session> implements ISessionRepository {

    @Override
    public void add(@NotNull final String userId, @NotNull final Session session) {
        session.setUserId(userId);
        records.add(session);
    }

    @Override
    public void remove(@NotNull final String userId, @NotNull final Session session) {
        if (!userId.equals(session.getUserId())) return;
        records.remove(session);
    }

    @Override
    public void clear(@NotNull final String userId) {
        records.removeAll(findAllSession(userId));
    }

    @Nullable
    @Override
    public List<Session> findAllSession(@NotNull final String userId) {
        final List<Session> result = new ArrayList<>();
        for (final Session session : records) {
            if (userId.equals(session.getUserId())) result.add(session);
        }
        return result;
    }

    @Nullable
    @Override
    public Session findByUserId(@NotNull final String userId) throws Exception {
        for (final Session session : records) {
            if (userId.equals(session.getUserId())) return session;
        }
        throw new Exception();
    }

    @Nullable
    @Override
    public Session findById(@NotNull final String id) throws Exception {
        for (final Session session : records) {
            if (id.equals(session.getId())) return session;
        }
        throw new Exception();
    }

    @Override
    public Session add(Session session) {
        if (session == null) return null;
        records.add(session);
        return session;
    }

    @Nullable
    @Override
    public Session removeByUserId(@NotNull final String userId) throws Exception {
        final Session session = findByUserId(userId);
        remove(userId, session);
        return session;
    }

    @Override
    public boolean contains(@NotNull final String id) throws Exception {
        final Session session = findById(id);
        return findAll().contains(session);
    }
}