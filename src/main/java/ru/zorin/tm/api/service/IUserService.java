package ru.zorin.tm.api.service;

import org.springframework.stereotype.Service;
import ru.zorin.tm.entity.Project;
import ru.zorin.tm.entity.User;
import ru.zorin.tm.role.Role;

import java.util.List;

public interface IUserService extends IService<User>{

    List<User> findAll();

    User create(String login, String password);

    User create(String login, String password, String email);

    User create(String login, String password, Role role);

    User findById(String userId);

    String getUserId();

    User findByLogin(String login);

    User updateById(String id, String login, String password);

    User updateByLogin(String login, String newLogin, String password);

    User removeUser(User user);

    User removeById(String id);

    User removeByLogin(String login);

    User lockServiceByLogin(String login);

    User unlockServiceByLogin(String login);
}