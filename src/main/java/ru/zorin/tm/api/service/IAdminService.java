package ru.zorin.tm.api.service;

import ru.zorin.tm.entity.Session;

public interface IAdminService {

    void loadData64() throws Exception;

    void saveData64() throws Exception;

    void clearData64() throws Exception;

    void loadBin() throws Exception;

    void saveBin() throws Exception;

    void clearBin() throws Exception;

    void loadFastJson() throws Exception;

    void saveFastJson() throws Exception;

    void clearFastJson() throws Exception;

    void loadFastXML() throws Exception;

    void saveFastXML() throws Exception;

    void clearFastXML() throws Exception;

    void loadJaxbJson() throws Exception;

    void saveJaxbJson() throws Exception;

    void clearJaxbJson() throws Exception;

    void loadJaxbXML() throws Exception;

    void saveJaxbXML() throws Exception;

    void clearJaxbXML() throws Exception;
}
