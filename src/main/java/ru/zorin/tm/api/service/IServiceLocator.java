package ru.zorin.tm.api.service;

import org.springframework.stereotype.Service;

@Service
public interface IServiceLocator {

    IUserService getUserService();

    IAuthService getAuthService();

    ITaskService getTaskService();

    IDomainService getDomainService();

    IProjectService getProjectService();

    IPropertyService getPropertyService();

    ISessionService getSessionService();

    IAdminService getAdminService();

}