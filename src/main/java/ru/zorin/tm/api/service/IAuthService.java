package ru.zorin.tm.api.service;

import org.springframework.stereotype.Service;
import ru.zorin.tm.entity.User;
import ru.zorin.tm.role.Role;

@Service
public interface IAuthService {

    String getUserId();

    User getUser(String login);

    boolean isAuth();

    void checkRole (Role[] roles);

    void login(String login, String password);

    void logout();

    void registry(String login, String password, String email);
}