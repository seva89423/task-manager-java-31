package ru.zorin.tm.api.endpoint;

import org.jetbrains.annotations.Nullable;
import ru.zorin.tm.entity.User;
import ru.zorin.tm.role.Role;

import javax.jws.WebMethod;
import javax.jws.WebParam;

public interface IAuthEndpoint {

    @WebMethod
    User getUser(@WebParam(name = "login", partName = "login") @Nullable String login) throws Exception;

    @WebMethod
    boolean isAuth();

    @WebMethod
    void logout();

    @WebMethod
    void login(@WebParam(name = "login", partName = "login") @Nullable String login, @WebParam(name = "password", partName = "password") @Nullable String password) throws Exception;

    @WebMethod
    void registry(@WebParam(name = "login", partName = "login") String login, @WebParam(name = "password", partName = "password") String password, @WebParam(name = "email", partName = "email") String email) throws Exception;

    @WebMethod
    void checkRole(@WebParam(name = "role", partName = "role") Role[] roles) throws Exception;
}