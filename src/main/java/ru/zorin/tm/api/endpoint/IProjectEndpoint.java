package ru.zorin.tm.api.endpoint;

import ru.zorin.tm.entity.Project;
import ru.zorin.tm.entity.Session;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import java.util.List;

public interface IProjectEndpoint {

    @WebMethod
    void createProject(@WebParam (name = "session", partName = "session") Session session, @WebParam(name = "name", partName = "name") String name) throws Exception;

    @WebMethod
    void addProject(@WebParam (name = "session", partName = "session") Session session, @WebParam(name = "project", partName = "project") Project project) throws Exception;

    @WebMethod
    void removeProject(@WebParam (name = "session", partName = "session") Session session, @WebParam(name = "project", partName = "project") Project project) throws Exception;

    @WebMethod
    List<Project> findAllProject(@WebParam (name = "session", partName = "session") Session session) throws Exception;

    @WebMethod
    void clearProject(@WebParam (name = "session", partName = "session") Session session) throws Exception;

    @WebMethod
    Project findProjectById(@WebParam (name = "session", partName = "session") Session session, @WebParam(name = "id", partName = "id") String id) throws Exception;

    @WebMethod
    Project findProjectByIndex(@WebParam (name = "session", partName = "session") Session session, @WebParam(name = "index", partName = "index") Integer index) throws Exception;

    @WebMethod
    Project findProjectByName(@WebParam (name = "session", partName = "session") Session session, @WebParam(name = "name", partName = "name") String name) throws Exception;

    @WebMethod
    Project removeProjectByName(@WebParam (name = "session", partName = "session") Session session, @WebParam(name = "name", partName = "name") String name) throws Exception;

    @WebMethod
    Project removeProjectByIndex(@WebParam (name = "session", partName = "session") Session session, @WebParam(name = "index", partName = "index") Integer index) throws Exception;

    @WebMethod
    Project removeProjectById(@WebParam (name = "session", partName = "session") Session session, @WebParam(name = "id", partName = "id") String id) throws Exception;

    @WebMethod
    Project updateProjectById(@WebParam (name = "session", partName = "session") Session session, @WebParam(name = "id", partName = "id") String id, String name, String description) throws Exception;

    @WebMethod
    Project updateProjectByIndex(@WebParam (name = "session", partName = "session") Session session, @WebParam(name = "index", partName = "index") Integer index, @WebParam(name = "name", partName = "name") String name, @WebParam(name = "description", partName = "description") String description) throws Exception;
}