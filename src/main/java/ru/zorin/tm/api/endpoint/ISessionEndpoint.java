package ru.zorin.tm.api.endpoint;

import org.jetbrains.annotations.Nullable;
import ru.zorin.tm.dto.Result;
import ru.zorin.tm.entity.Session;

import javax.jws.WebMethod;
import javax.jws.WebParam;

public interface ISessionEndpoint {

    @WebMethod
    Result closeSession(@WebParam(name = "session", partName = "session") @Nullable Session session) throws Exception;

    @WebMethod
    Result closeAllSession(@WebParam(name = "session", partName = "session") @Nullable Session session) throws Exception;

    @WebMethod
    @Nullable
    Session openSession(@WebParam(name = "session", partName = "session") @Nullable String login, @WebParam(name = "password", partName = "password") @Nullable String password) throws Exception;
}