package ru.zorin.tm.api.endpoint;

import ru.zorin.tm.dto.Result;
import ru.zorin.tm.entity.Session;

import javax.jws.WebMethod;
import javax.jws.WebParam;

public interface IAdminEndpoint {

    @WebMethod
    Result loadData64(@WebParam(name = "session", partName = "session") Session session) throws Exception;

    @WebMethod
    Result saveData64(@WebParam(name = "session", partName = "session") Session session) throws Exception;

    @WebMethod
    Result clearData64(@WebParam(name = "session", partName = "session") Session session) throws Exception;

    @WebMethod
    Result loadBin(@WebParam(name = "session", partName = "session") Session session) throws Exception;

    @WebMethod
    Result saveBin(@WebParam(name = "session", partName = "session") Session session) throws Exception;

    @WebMethod
    Result clearBin(@WebParam(name = "session", partName = "session") Session session) throws Exception;

    @WebMethod
    Result loadFastJson(@WebParam(name = "session", partName = "session") Session session) throws Exception;

    @WebMethod
    Result saveFastJson(@WebParam(name = "session", partName = "session") Session session) throws Exception;

    @WebMethod
    Result clearFastJson(@WebParam(name = "session", partName = "session") Session session) throws Exception;

    @WebMethod
    Result loadFastXML(@WebParam(name = "session", partName = "session") Session session) throws Exception;

    @WebMethod
    Result saveFastXML(@WebParam(name = "session", partName = "session") Session session) throws Exception;

    @WebMethod
    Result clearFastXML(@WebParam(name = "session", partName = "session") Session session) throws Exception;

    @WebMethod
    Result loadJaxbJson(@WebParam(name = "session", partName = "session") Session session) throws Exception;

    @WebMethod
    Result saveJaxbJson(@WebParam(name = "session", partName = "session") Session session) throws Exception;

    @WebMethod
    Result clearJaxbJson(@WebParam(name = "session", partName = "session") Session session) throws Exception;

    @WebMethod
    Result loadJaxbXML(@WebParam(name = "session", partName = "session") Session session) throws Exception;

    @WebMethod
    Result saveJaxbXML(@WebParam(name = "session", partName = "session") Session session) throws Exception;

    @WebMethod
    Result clearJaxbXML(@WebParam(name = "session", partName = "session") Session session) throws Exception;
}