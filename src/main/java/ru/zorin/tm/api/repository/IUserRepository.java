package ru.zorin.tm.api.repository;

import org.springframework.stereotype.Repository;
import ru.zorin.tm.entity.Project;
import ru.zorin.tm.entity.User;

import java.util.List;
@Repository
public interface IUserRepository {

    List<User> findAllUser();

    User add (User user);

    User findByUserId(String id);

    User findByUserLogin(String login);

    User removeUser(User user);

    User removeByUserId(String id);

    User removeByUserLogin(String login);

    List<User> getUsersList();

    void load(List<User> userList);

    void add(final List<User> userList);
}