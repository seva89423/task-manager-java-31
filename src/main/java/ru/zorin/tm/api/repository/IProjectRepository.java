package ru.zorin.tm.api.repository;

import org.springframework.stereotype.Repository;
import ru.zorin.tm.entity.Project;

import java.util.List;
@Repository
public interface IProjectRepository extends IRepository<Project> {

    void add(String userId, Project project);

    void add(final List<Project> project);

    void load(List<Project> projects);

    void remove(String userId, Project project);

    List<Project> findAllProjects(String userId);

    Project findProjectById(String userId, String id);

    Project findProjectByIndex(String userId, Integer index);

    Project getProjectByName(String userId, String name);

    Project removeProjectByName(String userId, String name);

    Project removeProjectByIndex(String userId, Integer index);

    Project removeProjectById(String userId, String id);
}
