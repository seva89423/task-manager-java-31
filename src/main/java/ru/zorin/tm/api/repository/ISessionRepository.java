package ru.zorin.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Repository;
import ru.zorin.tm.entity.Session;

import java.util.List;
@Repository
public interface ISessionRepository extends IRepository<Session>{

    void add(@NotNull final String userId, @NotNull final Session session);

    void remove(@NotNull final String userId, @NotNull final Session session);

    void clear(@NotNull final String userId);

    List<Session> findAllSession(@NotNull final String userId);

    Session findByUserId(@NotNull final String userId) throws Exception;

    Session findById(@NotNull final String id) throws Exception;

    Session removeByUserId(@NotNull final String userId) throws Exception;

    boolean contains(@NotNull final String id) throws Exception;
}
