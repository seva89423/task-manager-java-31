package ru.zorin.tm;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.stereotype.Service;
import ru.zorin.tm.bootstrap.Bootstrap;
import ru.zorin.tm.config.ApplicationConfig;

@Service
@SpringBootApplication
public class Application {

    public static void main(String[] args) throws Exception {
        final AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(ApplicationConfig.class);
        final Bootstrap bootstrap = context.getBean(Bootstrap.class);
        bootstrap.run(args);
    }
}